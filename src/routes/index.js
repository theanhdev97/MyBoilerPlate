import React, {Component} from 'react';
import {Router, Scene} from 'react-native-router-flux';
import Splash from '../containers/splash/index';
import Login from '../containers/login/index';
// import Login2 from '../containers/Login2';
import Login2 from '../containers/Login2';

// import {View} from 'react-native';
console.disableYellowBox = true;

// const Login2 = () => <View />;

class Routes extends Component {
  render () {
    return (
      <Router>
        <Scene key="root">
          <Scene key="SplashScreen" component={Splash} hideNavBar />
          {/* <Scene initial key="LoginScreen" component={Login} hideNavBar /> */}
          <Scene initial key="LoginScreen2" component={Login2} hideNavBar />
        </Scene>
      </Router>
    );
  }
}

export default Routes;
