import {
  UPDATE_TOKEN,
  UPDATE_USER_INFO,
  CLEAR_TOKEN,
  FETCH_DATA,
  CALL_CLEAR_TOKEN,
  CALL_UPDATE_TOKEN,
  CALL_UPDATE_USER_INFO,
} from './action_types';

action = (_type, _payload = {}) => {
  return {
    type: _type,
    payload: _payload,
  };
};

export const clearToken = () => action (CALL_CLEAR_TOKEN);

export const updateToken = _token =>
  action (CALL_UPDATE_TOKEN, {
    token: _token,
  });

export const updateUserInfo = _userInfo =>
  action (CALL_UPDATE_USER_INFO, {
    userInfo: _userInfo,
  });