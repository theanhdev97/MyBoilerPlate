export const UPDATE_TOKEN = 'update_token';
export const CLEAR_TOKEN = 'clear_token';
export const UPDATE_USER_INFO = 'update_user_info';

export const CALL_UPDATE_TOKEN = 'call_update_token';
export const CALL_CLEAR_TOKEN = 'call_clear_token';
export const CALL_UPDATE_USER_INFO = 'call_update_user_info';
