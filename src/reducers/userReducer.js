import {
  UPDATE_TOKEN,
  UPDATE_USER_INFO,
  CLEAR_TOKEN,
} from '../actions/action_types';

const initialState = {
  token: '',
  userInfo: {},
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_TOKEN:
      return {
        ...state,
        token: '',
      };
    case UPDATE_TOKEN:
      return {
        ...state,
        token: action.token,
      };
    case UPDATE_USER_INFO:
      return {
        ...state,
        userInfo: action.userInfo,
      };
    default:
      return state;
  }
};

export default userReducer;
