import { combineEpics } from 'redux-observable';
import fetchEpic from './fetchEpic';

const rootEpic = combineEpics(fetchEpic);
export default rootEpic;
