import PropTypes from "prop-types";
import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { ImageLoad } from "react-native-image-placeholder";
import BaseLoadMoreList from "../../base/LoadMoreList/index";
import Ripple from "react-native-material-ripple";
import { Rating } from "react-native-elements";

import { Card, Icon } from "native-base";
import { getPlaces, getPlacesNearby } from "../../api/place.js";

let context;

export default class ListPlaces extends Component {
	static propTypes = {
		onPressItem: PropTypes.func,
		type: PropTypes.any,
		latitude: PropTypes.any,
		longtitude: PropTypes.any,
		isNearby: PropTypes.bool
	};

	static defaultProps = {
		isNearby: false
	};

	constructor(props) {
		super(props);
		context = this;
	}

	onPressItem = item => {
		context.props.onPressItem(item);
	};

	renderItem = item => (
		<Ripple
			rippleCentered={true}
			onPress={() => this.onPressItem(item)}
			rippleDuration={1000}
		>
			<Card
				style={{
					flexDirection: "row",
					justifyContent: "center",
					alignItems: "center",
					height: 130,
					// borderRadius: 20,
					padding: 10
				}}
			>
				<Image
					source={{ uri: item.images["0"] }}
					style={{
						height: 100,
						width: 100,
						borderRadius: 50
					}}
				/>
				<View
					style={{ flex: 1, padding: 15, justifyContent: "center" }}
				>
					<Text
						style={{
							color: "",
							fontSize: 18,
							fontWeight: "bold"
						}}
						numberOfLines={1}
						ellipsizeMode={"tail"}
					>
						{item.title}
					</Text>

					<View
						style={{
							flexDirection: "row",
							marginTop: 4,
							alignItems: "center"
						}}
						// <Icon
						// 	name="star"
						// 	type="FontAwesome"
						// 	style={{
						// 		color: "#ECB825",
						// 		fontSize: 20
						// 	}}
						// />
					>
						<Rating
							type="star"
							ratingCount={5}
							fractions={1}
							startingValue={item.rating}
							imageSize={13}
							// onFinishRating={this.ratingCompleted}
							// showRating
							ratingColor="black"
							style={{ paddingVertical: 0 }}
						/>
						<Text
							style={{
								marginLeft: 5,
								color: "#4ab8b8",
								fontSize: 14,
								fontWeight: "900"
							}}
						>
							{item.rating}
						</Text>
						<Text
							style={{
								color: "gray",
								fontWeight: "bold",
								fontSize: 10,
								marginLeft: 5
							}}
						>
							( {item.totalRating} reivews )
						</Text>
					</View>
					<Text
						style={{
							marginTop: 8,
							color: "black",
							fontSize: 12
						}}
						numberOfLines={2}
						ellipsizeMode={"tail"}
					>
						{item.description}
					</Text>
					<View
						style={{
							marginTop: 5,
							flexDirection: "row",
							alignItem: "center"
						}}
					>
						<Icon
							name={"map"}
							type="FontAwesome"
							style={{ fontSize: 10, color: "gray" }}
						/>
						<Text
							style={{
								fontSize: 10,
								color: "black",
								marginLeft: 7,
								fontWeight: "bold"
							}}
						>
							{item.distance} km
						</Text>
					</View>
				</View>
			</Card>
		</Ripple>
	);

	fetchData = (_page, _onSuccess, _onFailure) => {
		let { type, latitude, longtitude,isNearby } = context.props;
		if (!isNearby)
			getPlaces(
				type,
				_page,
				latitude,
				longtitude,
				_onSuccess,
				_onFailure
			);
		else
			getPlacesNearby(
				type,
				_page,
				latitude,
				longtitude,
				_onSuccess,
				_onFailure
			);
	};

	render() {
		return (
			<BaseLoadMoreList
				ref={ref => (this.loadMoreList = ref)}
				initialPage={this.props.initialPage}
				fetchData={this.fetchData}
				renderItem={this.renderItem}
			/>
		);
	}
}
