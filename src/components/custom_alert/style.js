import { StyleSheet, Platform } from "react-native";

export default StyleSheet.create({
	parent_container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	child_container: {
		backgroundColor: "white",
		height: Platform.select({ android: 150, ios: 130 }),
		width: 300,
		borderRadius: 15,
		borderWidth: 5,
		borderColor: "white",
		alignItems: "center"
	},
	dialog_title: {
		marginTop: 15,
		fontSize: 20,
		fontWeight: "bold"
	},
	dialog_message: { color: "black", marginTop: 5 },
	divider: {
		marginTop: 15,
		marginBottom: 15,
		height: 0.5,
		width: "100%",
		backgroundColor: "gray",
		justifyContent: "center"
	},
	dialog_button: {
		flex: 1,
		alignItems: "center"
	},
	dialog_button_text: {
		width: 250,
		textAlign: "center",
		color: "#1464FA",
		fontWeight: "bold",
		fontSize: 20
	}
});
