import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity, Platform } from "react-native";
import { Button } from "native-base";
import Spinner from "react-native-loading-spinner-overlay";
import style from "./style";

class CustomAlert extends PureComponent {
	render() {
		return (
			<Spinner visible={this.props.visible} cancelable={false}>
				<View
					// style={{
					// 	flex: 1,
					// 	justifyContent: "center",
					// 	alignItems: "center"
					// }}
					style={style.parent_container}
				>
					<View
						// style={{
						// 	backgroundColor: "white",
						// 	height: Platform.select({ android: 150, ios: 130 }),
						// 	width: 300,
						// 	borderRadius: 15,
						// 	borderWidth: 5,
						// 	borderColor: "white",
						// 	alignItems: "center"
						// }}
						style={style.child_container}
					>
						<Text style={style.dialog_title}>
							{this.props.title}
						</Text>
						<Text style={style.dialog_message}>
							{this.props.message}
						</Text>
						<View style={style.divider} />
						<TouchableOpacity
							// style={{
							// 	flex: 1,
							// 	alignItems: "center"
							// 	// backgroundColor: "pink"
							// }}
							style={style.dialog_button}
							onPress={this.props.onPressOK}
						>
							<Text
								// style={{
								// 	width: 250,
								// 	textAlign: "center",
								// 	color: "#1464FA",
								// 	fontWeight: "bold",
								// 	fontSize: 20
								// 	// marginBottom: Platform.select({
								// 	// 	android: 10,
								// 	// 	ios: 0
								// 	// })
								// }}
								style={style.dialog_button_text}
							>
								OK
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Spinner>
		);
	}
}

export default CustomAlert;
