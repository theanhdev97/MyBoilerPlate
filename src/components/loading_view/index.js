import React, { Component } from "react";
import { View } from "react-native";
import { PulseIndicator } from "react-native-indicators";

const LoadingView = () => (
	<View
		style={{
			height: 120,
			justifyContent: "center",
			alignItems: "center"
		}}
	>
		<PulseIndicator color="#59D0EB" size={80} />
	</View>
);

export default LoadingView;
