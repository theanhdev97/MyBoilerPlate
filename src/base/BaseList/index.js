import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {View, FlatList} from 'react-native';

export default class BaseList extends Component {
  static propTypes = {
    numColumns: PropTypes.number,
    data: PropTypes.array,
    renderItem: PropTypes.func,
  };

  static defaultProps = {
    numColumns: 1,
  };

  render () {
    let {data} = this.props;
    return (
      <View>
        <FlatList
          //   numColumns={numColumns}
          data={data}
          extraData={this.props}
          renderItem={({item}) => this.props.renderItem (item)}
          keyExtractor={item => item}
        />
      </View>
    );
  }
}
