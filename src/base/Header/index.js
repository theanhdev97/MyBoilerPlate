import PropTypes from "prop-types";
import { Header, Left, Icon, Body, Text, Right } from "native-base";
import { TouchableOpacity } from "react-native";
import React, { PureComponent } from "react";

export default class BaseHeader extends PureComponent {
	static propTypes = {
		// attr
		title: PropTypes.string,
		titleStyle: PropTypes.object,
		leftIcon: PropTypes.string,
		rightIcon: PropTypes.string,
		iconColor: PropTypes.string,
		iconSize: PropTypes.number,
		backgroundColor: PropTypes.string,

		// function
		onPressLeftMenu: PropTypes.func,
		onPressRightMenu: PropTypes.func
	};

	static defaultProps = {
		title: "",
		titleStyle: {},
		leftIcon: "",
		rightIcon: "",
		titleColor: "",
		iconColor: "",
		titleSize: 20,
		iconSize: 20
	};

	render() {
		let {
			title,
			titleStyle,
			leftIcon,
			rightIcon,
			iconColor,
			iconSize,
			backgroundColor
		} = this.props;
		return (
			<Header style={{ backgroundColor: backgroundColor }}>
				{leftIcon === "" ? (
					<Left />
				) : (
					<Left
						style={{ flexDirection: "row", alignItems: "center",marginLeft:8 }}
					>
						<TouchableOpacity onPress={this.props.onPressLeftMenu}>
							<Icon
								name={leftIcon}
								type="FontAwesome"
								style={{ fontSize: iconSize, color: iconColor }}
							/>
						</TouchableOpacity>
					</Left>
				)}
				<Body>
					<Text style={titleStyle}>{this.props.title}</Text>
				</Body>
				{rightIcon === "" ? (
					<Right />
				) : (
					<Right>
						<TouchableOpacity onPress={this.props.onPressRightMenu}>
							<Icon
								name={rightIcon}
								type="FontAwesome"
								style={{ fontSize: iconSize, color: iconColor }}
							/>
						</TouchableOpacity>
					</Right>
				)}
			</Header>
		);
	}
}
