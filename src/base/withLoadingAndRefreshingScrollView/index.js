import React, {Component} from 'react';
import {RefreshControl, ScrollView} from 'react-native';

/*
    _fetchAPI(_onFetchDataSuccess, _onFetchDataFailure) : 
*/

function withLoadingAndFetchingData (
  _Comp,
  _fetchAPI,
  _callBackOnFetchData = false
) {
  return class HOC extends Component {
    constructor (props) {
      super (props);
      this.state = {
        refreshing: false,
        data: null,
      };
    }

    componentDidMount () {
      this.showLoading (true);
      _fetchAPI (this.onFetchDataSuccess, this.onFetchDataFailure);
    }

    onRefresh = () => {
      this.showLoading (true);
      _fetchAPI (this.onFetchDataSuccess, this.onFetchDataFailure);
    };

    onFetchDataSuccess = data => {
      this.showLoading (false);
      this.setState ({
        data: data,
      });

      _callBackOnFetchData && this.cComponent.onFetchDataSuccess ();
    };

    onFetchDataFailure = error => {
      this.showLoading (false);
      alert (error);

      _callBackOnFetchData && this.cComponent.onFetchDataFailure ();
    };

    showLoading = _show => {
      this.setState ({refreshing: _show});
    };

    render () {
      let {refreshing, data} = this.state;
      return (
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.onRefresh}
            />
          }
        >
          {data &&
            <_Comp
              data={data}
              {...this.props}
              ref={ref => (this.cComponent = ref)}
            />}
        </ScrollView>
      );
    }
  };
}

export default withLoadingAndFetchingData;
