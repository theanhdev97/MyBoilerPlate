import PropTypes from "prop-types";
import React, { Component, PureComponent } from "react";
import {
	TouchableOpacity,
	View,
	Text,
	FlatList,
	ActivityIndicator,
	Dimensions
} from "react-native";
// import ImageLoad from "react-native-image-placeholder";
import { Container, Toast, Icon } from "native-base";
// import * as DateTimeHelper from "../utils/DateTimeHelper";
let context;

// function
// refreshData() => call to refresh data

class LoadMoreList extends Component {
	static propTypes = {
		fetchData: PropTypes.func,
		refreshable: PropTypes.bool,
		renderItem: PropTypes.func,
		initialPage: PropTypes.number,
		limitPerPage: PropTypes.number
	};

	static defaultProps = {
		initialPage: 0,
		refreshable: true,
		limitPerPage: 10
	};

	constructor(props) {
		super(props);
		context = this;
		this.state = {
			listNews: [],
			isEmpty: false,
			loading: false,
			isShowNetworkError: false,
			isFirstLoad: true,
			isShowLoadmore: false
		};
		// bind function to optimize
		this.mData = [];
		this.mPage = props.initialPage;
		this.mLoading = false;
		this.mRefreshing = false;
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (
			this.state.isEmpty != nextState.isEmpty ||
			this.state.loading != nextState.loading ||
			this.state.isShowNetworkError != nextState.isShowNetworkError ||
			this.state.isShowLoadmore != nextState.isShowLoadmore
		)
			return true;
		return false;
	}

	componentDidMount() {
		context.fetchData();
	}

	componentWillUnmount() {
		context.setState({
			listNews: undefined,
			isEmpty: undefined,
			loading: undefined,
			isShowNetworkError: undefined,
			isFirstLoad: undefined,
			isShowLoadmore: undefined
		});
	}

	fetchData() {
		let { initialPage } = context.props;
		context.setState({ isShowNetworkError: false });
		if (context.mLoading == false) {
			// set flag fetching data
			context.mLoading = true;
			context.setState({ loading: true });

			if (context.mPage > initialPage)
				context.setState({ isShowLoadmore: true });

			this.props.fetchData(
				context.mPage,
				this.onFetchDataSuccess,
				this.onFetchDataFailure
			);
		}
	}

	refreshData = () => {
		context.onRefresh();
	};

	onFetchDataSuccess(_data, _page) {
		console.log(_data);
		console.log("Page :  " + _page);
		let { initialPage } = context.props;
		// dismiss add new datas when refreshing
		// if (page == 1 && _page != 1) return;
		if (context.mRefreshing == true) {
			if (_page != initialPage) {
				return;
			}
		}

		if (_page == initialPage) context.mData = [];

		context.mData.push(..._data);
		context.mRefreshing = false;
		context.mLoading = false;
		// handle
		context.mPage += 1;
		context.setState({
			listNews: context.mData,
			loading: false,
			isFirstLoad: false,
			isShowLoadmore: false
		});
	}

	// onFetchDataSuccess(_news, _page) {
	// 	let { initialPage } = context.props;
	// 	// dismiss add new datas when refreshing
	// 	// if (page == 1 && _page != 1) return;
	// 	if (context.mRefreshing == true) {
	// 		if (_page != initialPage) {
	// 			return;
	// 		}
	// 	}

	// 	if (_page == initialPage) context.mData = [];
	// 	context.mData.push(..._news);
	// 	context.mRefreshing = false;
	// 	context.mLoading = false;
	// 	// handle
	// 	context.mPage += 1;
	// 	context.setState({
	// 		listNews: context.mData,
	// 		loading: false,
	// 		isFirstLoad: false,
	// 		isShowLoadmore: false
	// 	});
	// }

	onFetchDataFailure(error) {
		context.mLoading = false;
		context.mRefreshing = false;

		context.setState({
			loading: false,
			isFirstLoad: false
		});

		if (context.state.isShowLoadmore)
			context.setState({ isShowLoadmore: false });

		if (page == context.props.initialPage) {
			context.setState({ isShowNetworkError: true });
		} else {
			Toast.show({
				text: "Lỗi kết nối mạng"
			});
		}
	}

	handleEmptyData() {
		context.mLoading = false;
		context.mRefreshing = false;
		context.setState({
			loading: false,
			isFirstLoad: false,
			isEmpty: true
			// isShowLoadmore: false
		});
		if (context.state.isShowLoadmore)
			context.setState({ isShowLoadmore: false });
	}

	onRefresh() {
		console.log("On Refresh");
		context.mPage = context.props.initialPage;
		context.mData = [];
		context.mRefreshing = true;
		context.mLoading = false;
		context.fetchData();

		context.setState({
			listNews: [],
			isEmpty: false,
			isFirstLoad: true
			// isShowLoadmore: false
		});
		if (context.state.isShowLoadmore)
			context.setState({ isShowLoadmore: false });
	}

	handleLoadMore() {
		let { limitPerPage } = context.props;
		if (
			context.mData &&
			context.mData.length > 0 &&
			context.state.isEmpty == false &&
			// listNews.length % 10 == 0
			context.mData.length % limitPerPage == 0
		) {
			context.fetchData();
		}
	}

	render() {
		let { loading, isShowNetworkError } = context.state;
		return (
			<Container>
				{context.renderFlatList()}
				{isShowNetworkError ? context._renderNetworkError() : null}
			</Container>
		);
	}

	renderFlatList() {
		return (
			<FlatList
				refreshing={false}
				onRefresh={context.onRefresh}
				data={context.state.listNews}
				extraData={context.state}
				keyExtractor={item => item.title}
				onEndReachedThreshold={100}
				onEndReached={context.handleLoadMore}
				ListEmptyComponent={context.renderShimmerLoadingEmpty}
				ListFooterComponent={context.renderLoadMoreFooter}
				renderItem={({ item }) => context.props.renderItem(item)}
				removeClippedSubviews={true}
			/>
		);
	}

	renderShimmerLoadingEmpty() {
		let { isFirstLoad } = context.state;
		return isFirstLoad ? <ShimmerLoadingEmpty /> : null;
	}

	renderLoadMoreFooter() {
		let { isShowLoadmore } = context.state;
		return <LoadingFooter isVisible={isShowLoadmore} />;
	}

	_renderNetworkError() {
		return (
			<View>
				<TouchableOpacity
					style={{
						height: 50,
						marginLeft: 10,
						marginRight: 10,
						width: Dimensions.get("window").width - 20,
						flexDirection: "row",
						justifyContent: "space-between",
						paddingLeft: 10,
						paddingRight: 10,
						paddingTop: 10,
						paddingBottom: 10,
						backgroundColor: "gray",
						opacity: 0.5,
						position: "absolute",
						bottom: 10,
						alignItems: "center",
						borderRadius: 3,
						borderWidth: 3,
						borderColor: "transparent"
					}}
					onPress={context.onRefresh}
				>
					<Text
						style={{
							color: "white",
							fontWeight: "bold",
							fontSize: 16
						}}
					>
						Lỗi kết nối mạng
					</Text>

					<View
						style={{
							flexDirection: "row",
							backgroundColor: "transparent",
							alignItems: "center"
						}}
					>
						<Text
							style={{
								color: "white",
								fontWeight: "bold",
								fontSize: 16,
								marginRight: 10
							}}
						>
							Tải lại
						</Text>
						<Icon
							type="material-community"
							name="reload"
							color="white"
							size={30}
						/>
					</View>
				</TouchableOpacity>
			</View>
		);
	}
}

class ShimmerLoadingEmpty extends PureComponent {
	render() {
		return (
			<View style={{ backgroundColor: "gray" }}>
				<Text>Loading</Text>
			</View>
		);
	}
}

class LoadingFooter extends PureComponent {
	render() {
		let { isVisible } = this.props;

		return isVisible ? (
			<View
				style={{
					height: 60,
					backgroundColor: "white",
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<ActivityIndicator size="large" color="gray" />
			</View>
		) : null;
	}
}

export default LoadMoreList;
