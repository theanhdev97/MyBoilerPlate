import { StyleSheet } from "react-native";

export default StyleSheet.create({
	tabHeadingContainer: {
		flexDirection: "column",
		// backgroundColor: "#a8e6cf"
		backgroundColor: "white",
		borderTopColor: "gray",
		borderBottomColor: "white",
		borderLeftColor: "white",
		borderRightColor: "white",
		// elevation: 2
		// borderWidth: 0.3
		shadowColor: "#000",
		shadowOffset: { width: 8, height: 2 },
		shadowOpacity: 0.3,
		elevation: 2
	}
});
