import PropTypes from "prop-types";
import React, { Component } from "react";
import { View } from "react-native";
import { Tabs, Tab, TabHeading, Text, Icon } from "native-base";

class BaseTab extends Component {
	static propTypes = {
		//  attr
		initialPage: PropTypes.number,
		tabStyle: PropTypes.object,
		activeTabStyle: PropTypes.object,
		activeColor: PropTypes.string,
		unActiveColor: PropTypes.string,
		tabBarPosition: PropTypes.oneOf("bottom", "top"),
		tabBarUnderlineStyle: PropTypes.object,
		titleStyle: PropTypes.object,
		iconStyle: PropTypes.object,
		activeIconStyle: PropTypes.object,
		activeTitleStyle: PropTypes.object,
		hasIcon: PropTypes.bool,
		hasTitle: PropTypes.bool,
		lockable: PropTypes.bool,

		// data
		elements: PropTypes.array.isRequired,
		titles: PropTypes.array.isRequired,
		icons: PropTypes.array.isRequired,

		// function
		onChangeTab: PropTypes.func.isRequired
	};

	static defaultProps = {
		initialPage: 0,
		tabBarPosition: "bottom",
		tabStyle: {
			flexDirection: "column",
			// backgroundColor: "#a8e6cf"
			backgroundColor: "white",
			borderTopColor: "gray",
			borderBottomColor: "white",
			borderLeftColor: "white",
			borderRightColor: "white",
			// elevation: 2
			// borderWidth: 0.3
			shadowColor: "#000",
			shadowOffset: { width: 8, height: 2 },
			shadowOpacity: 0.3,
			elevation: 2
		},
		activeTabStyle: {
			flexDirection: "column",
			// backgroundColor: "#a8e6cf"
			backgroundColor: "white",
			borderTopColor: "gray",
			borderBottomColor: "white",
			borderLeftColor: "white",
			borderRightColor: "white",
			// elevation: 2
			// borderWidth: 0.3
			shadowColor: "#000",
			shadowOffset: { width: 8, height: 2 },
			shadowOpacity: 0.3,
			elevation: 2
		},
		tabBarUnderlineStyle: {
			backgroundColor: "transparent"
		},
		iconStyle: {
			fontSize: 20
		},
		activeIconStyle: {
			fontSize: 30
		},
		titleStyle: {
			fontSize: 12
		},
		activeTitleStyle: {
			fontSize: 16,
			fontWeight: "bold"
		},
		activeColor: "black",
		unActiveColor: "gray",
		hasIcon: true,
		hasTitle: true,
		lockable: false,
		elements: [],
		titles: [],
		icons: []
	};

	constructor(props) {
		super(props);
		this.state = {
			activeTabIndex: props.initialPage
		};
	}

	componentWillUnmount() {
		this.setState({
			activeTabIndex: undefined
		});
		this._handleOnChangeTab = undefined;
	}

	onChangeTab = tab => {
		const index = tab.i;
		this.setState({
			activeTabIndex: index
		});
		this.props.onChangeTab(index);
	};

	renderTabItem = (element, icon, title, index) => {
		let {
			activeColor,
			unActiveColor,
			activeIconStyle,
			activeTitleStyle,
			titleStyle,
			iconStyle,
			hasTitle,
			hasIcon,
			tabStyle,
			activeTabStyle
		} = this.props;
		let { activeTabIndex } = this.state;

		return (
			<Tab
				heading={
					<TabHeading
						style={
							activeTabIndex === index ? activeTabStyle : tabStyle
						}
					>
						{hasIcon && (
							<Icon
								style={[
									activeTabIndex === index
										? activeIconStyle
										: iconStyle,
									{
										color:
											activeTabIndex == index
												? activeColor
												: unActiveColor
									}
								]}
								name={icon}
								type="FontAwesome"
							/>
						)}
						{hasTitle && (
							<Text
								style={[
									activeTabIndex === index
										? activeTitleStyle
										: titleStyle,
									{
										color:
											this.state.activeTabIndex == index
												? activeColor
												: unActiveColor
									}
								]}
							>
								{title}
							</Text>
						)}
					</TabHeading>
				}
			>
				{element}
			</Tab>
		);
	};

	render() {
		let { elements, titles, icons, lockable } = this.props;
		return (
			<Tabs
				style={{ borderTopColor: "gray", borderWidth: 1 }}
				initialPage={0}
				locked={lockable}
				onChangeTab={this.onChangeTab}
				tabBarPosition="bottom"
				tabBarUnderlineStyle={{
					backgroundColor: "transparent"
				}}
			>
				{elements.map((element, index) =>
					this.renderTabItem(
						element,
						icons[index],
						titles[index],
						index
					)
				)}
			</Tabs>
		);
	}
}

export default BaseTab;
