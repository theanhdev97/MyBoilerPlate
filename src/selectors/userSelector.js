import {createSelector} from 'reselect';
const tokenSelector = state => state.user.token;
const userInfoSelector = state => state.user.userInfo;

export const getToken = createSelector ([tokenSelector], token => {
  return token;
});

export const getUserInfo = createSelector ([userInfoSelector], userInfo => {
  return userInfo;
});
