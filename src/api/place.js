async function getPlaces(
	_type = 1,
	_page = 0,
	_latitude,
	_longtitude,
	_onSuccess,
	_onFailure
) {
	try {
		let details = {
			page: _page,
			type: _type,
			latitude: _latitude,
			longtitude: _longtitude
		};

		let formBody = [];
		for (let property in details) {
			let encodedKey = encodeURIComponent(property);
			let encodedValue = encodeURIComponent(details[property]);
			formBody.push(encodedKey + "=" + encodedValue);
		}
		formBody = formBody.join("&");
		let config = {
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded"
			},
			body: formBody
		};
		// let response = await fetch("http://localhost:8080/api/places/", config);
		let response = await fetch("http://192.168.3.22:8080/api/places/", config);
		let responseJson = await response.json();
		console.log(responseJson);

		// error request
		if (!responseJson.success) {
			// console.log(response.message);
			_onFailure(responseJson.message);
			console.log(obj);
		}
		// success request
		else {
			console.log(responseJson.data.data);
			_onSuccess(responseJson.data.data, responseJson.data.current_page);
		}
	} catch (error) {
		console.log(error);
		_onFailure(error);
	}
}

async function getPlacesNearby(
	_type = 1,
	_page = 0,
	_latitude,
	_longtitude,
	_onSuccess,
	_onFailure
) {
	try {
		let details = {
			page: _page,
			type: _type,
			latitude: _latitude,
			longtitude: _longtitude
		};

		let formBody = [];
		for (let property in details) {
			let encodedKey = encodeURIComponent(property);
			let encodedValue = encodeURIComponent(details[property]);
			formBody.push(encodedKey + "=" + encodedValue);
		}
		formBody = formBody.join("&");
		let config = {
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded"
			},
			body: formBody
		};
		let response = await fetch(
			// "http://localhost:8080/api/placesnearby",
			"http://192.168.3.22:8080/api/placesnearby",
			config
		);
		let responseJson = await response.json();
		console.log(responseJson);

		// error request
		if (!responseJson.success) {
			// console.log(response.message);
			_onFailure(responseJson.message);
		}
		// success request
		else {
			_onSuccess(responseJson.data.data, responseJson.data.current_page);
		}
	} catch (error) {
		console.log(error);
		// _onFailure(error);
	}
}

async function getPlacesWithCategory(
	_type = 1,
	_page = 0,
	_latitude,
	_longtitude,
	_onSuccess,
	_onFailure
) {
	try {
		let details = {
			page: _page,
			type: _type,
			latitude: _latitude,
			longtitude: _longtitude
		};

		let formBody = [];
		for (let property in details) {
			let encodedKey = encodeURIComponent(property);
			let encodedValue = encodeURIComponent(details[property]);
			formBody.push(encodedKey + "=" + encodedValue);
		}
		formBody = formBody.join("&");
		let config = {
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded"
			},
			body: formBody
		};
		let response = await fetch(
			"http://localhost:8080/api/placesnearby",
			config
		);
		let responseJson = await response.json();
		console.log(responseJson);

		// error request
		if (!responseJson.success) {
			// console.log(response.message);
			_onFailure(responseJson.message);
		}
		// success request
		else {
			_onSuccess(responseJson.data.data, responseJson.data.current_page);
		}
	} catch (error) {
		console.log(error);
		// _onFailure(error);
	}
}

export { getPlaces, getPlacesNearby, getPlacesWithCategory };
