async function getCommentsOfPlace(_page = 0, _placeID, _onSuccess, _onFailure) {
	try {
		let details = {
			page: _page,
			place_id: _placeID
		};

		let formBody = [];
		for (let property in details) {
			let encodedKey = encodeURIComponent(property);
			let encodedValue = encodeURIComponent(details[property]);
			formBody.push(encodedKey + "=" + encodedValue);
		}
		formBody = formBody.join("&");
		let config = {
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded"
			},
			body: formBody
		};
		let response = await fetch(
			"http://localhost:8080/api/commentsofplace",
			config
		);
		let responseJson = await response.json();
		console.log(responseJson);

		// error request
		if (!responseJson.success) {
			// console.log(response.message);
			_onFailure(responseJson.message);
		}
		// success request
		else {
			_onSuccess(responseJson.data.data, responseJson.data.current_page);
		}
	} catch (error) {
		console.log(error);
		_onFailure(error);
	}
}

async function addComment(
	_token,
	_userID,
	_placeID,
	_comment,
	_time,
	_onSuccess,
	_onFailure
) {
	try {
		let details = {
			user_id: _userID,
			place_id: _placeID,
			comment: _comment,
			time: _time
		};

		let formBody = [];
		for (let property in details) {
			let encodedKey = encodeURIComponent(property);
			let encodedValue = encodeURIComponent(details[property]);
			formBody.push(encodedKey + "=" + encodedValue);
		}
		formBody = formBody.join("&");
		let config = {
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded",
				token: _token
			},
			body: formBody
		};
		let response = await fetch(
			"http://localhost:8080/api/addcomment",
			config
		);
		let responseJson = await response.json();
		console.log(responseJson);

		// error request
		if (!responseJson.success) {
			// console.log(response.message);
			_onFailure(responseJson.message);
		}
		// success request
		else {
			console.log(responseJson);
			console.log(responseJson.data);
			_onSuccess(responseJson.data);
		}
	} catch (error) {
		// console.log(error);
		_onFailure(error);
	}
}

export { getCommentsOfPlace, addComment };
