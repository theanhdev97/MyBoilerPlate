import { StyleSheet } from "react-native";

export default StyleSheet.create({
	container: {
		// backgroundColor: "#bad7df"
		// backgroundColor: "#a8e6cf",
		backgroundColor: "white",
		flex: 1
	},
	header: {
		backgroundColor: "white",
		// backgroundColor: "#56AFF8",
		shadowColor: "#5bc4ff",
		shadowOpacity: 0,
		shadowOffset: {
			height: 0
		},
		shadowRadius: 0,
		elevation: 0,
		elevation: 0, //remove shadow on Android
		shadowOpacity: 0 //remove shadow on iOS,

		// 		shadowColor: "#000",
		// shadowOffset: { width: 0, height: 2 },
		// shadowOpacity: 1,
	},
	headerTitle: {
		color: "#56AFF8",
		// color: "white",
		width: 200,
		fontWeight: "bold",
		letterSpacing: 1.2
	},
	textItemTypeCoin: {
		textAlign: "center",
		color: "#21AEE9",
		fontSize: 18
	},
	buttonItemTypeCoin: {
		width: 200,
		justifyContent: "center"
	},
	typeCoinDialog: {
		flex: 1,
		justifyContent: "center"
	}
});
