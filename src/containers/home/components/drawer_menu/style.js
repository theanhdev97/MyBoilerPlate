import { StyleSheet } from "react-native";
import { Platform } from "react-native";

export default StyleSheet.create({
	container: {
		height: "100%",
		// width: "100%"
		width: 300
	},
	headerContainer: {
		width: "100%",
		backgroundColor: "#3366CC",
		alignItems: "center",
		justifyContent: "center",
		height: Platform.select({ android: 56, ios: 60 })
	},
	headerImage: { width: 230, height: 30 },
	headerIcon: {
		height: 60,
		width: "100%",
		resizeMode: "stretch"
	},
	headerTitle: {
		padding: 10,
		fontSize: 30,
		fontWeight: "900",
		letterSpacing: 2,
		color: "#FFFDE7"
	},
	menuItemTitle: {
		fontSize: 20,
		color:'white',
		marginLeft:16
	},
	flatlistContainer: {
		// backgroundColor: "white"
		backgroundColor: "#f73859"
	},
	copyRightContainer: {
		flex: 1,
		backgroundColor: "white",
		width: "100%",
		position: "absolute",
		bottom: 10,
		left: 0,
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "row"
	},
	copyRightText: { textAlign: "center", padding: 8, fontSize: 14 }
});
