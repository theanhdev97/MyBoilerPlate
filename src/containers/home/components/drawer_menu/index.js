import React, { PureComponent } from "react";
import { View, Dimensions } from "react-native";
import { List, Text } from "native-base";
import { ListItem } from "react-native-paper";
import { Icon } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";

// import { MENUS } from "../../../../../utils/constants";
import { MENUS } from "../../../../utils/constants";
import style from "./style";

// import { bindActionCreators } from "redux";
// import * as Action from "../../actions"; //Import your actions

// const IC_LOGO = require("../../../../../assets/image/ic_logo_drawer_menu.png");
// const BACKGROUND = require("../../../../../assets/image/header_drawer_menu.jpg");

class DrawerMenu extends PureComponent {
	_hanleOnPressMenuItem = item => {
		switch (item) {
			case "Log out":
				Actions.pop();
				break;
			default:
				alert("This feature is comming soon");
				break;
		}
	};

	render() {
		return (
			// {this._renderHeader()}
			<View style={style.container}>
				<View style={{ backgroundColor: "white", flex: 1 }}>
					<View style={style.flatlistContainer}>
						{this._renderFlatListMenu()}
					</View>
				</View>
			</View>
			// {this._renderCopyright()}
		);
	}

	// _renderHeader = () => (
	// 	<View style={style.headerContainer}>
	// 		<Image
	// 			source={require('../../../../../assets/image/background_header_drawer_menu.png')}
	// 			style={style.headerImage}
	// 			resizeMode="stretch"
	// 		/>
	// 	</View>
	// );

	_renderFlatListMenu = () => (
		<List
			dataArray={MENUS}
			renderRow={item => this._renderMenuItem(item)}
		/>
	);

	_renderMenuItem = item => (
		<View
			style={{
				height: Dimensions.get("window").height / MENUS.length,
				width: "100%"
			}}
		>
			<View
				style={{
					flex: 1,
					flexDirection: "row",
					alignItems: "center",
					paddingLeft: 16
				}}
			>
				<View
					style={{
						width:50,
						height:50,
						alignItems: "center",
						justifyContent: 'center' 
					}}
				>
					<Icon
						color="white"
						name={item.icon.name}
						type={item.icon.type}
						size={30}
					/>
				</View>
				<Text style={style.menuItemTitle}>{item.name}</Text>
			</View>
			<View
				style={{ height: 0.5, width: "100%", backgroundColor: "white" }}
			/>
		</View>
	);

	// _renderCopyright = () => (
	// 	<View style={style.copyRightContainer}>
	// 		<Icon name="copyright" size={15} type="material-community" />
	// 		<Text style={style.copyRightText}>
	// 			2018 - Powered by Nato Exchange
	// 		</Text>
	// 	</View>
	// );
}

function mapStateToProps(state, props) {
	return {
		totalBalance: state.coinReducer.totalBalance
	};
}
export default connect(
	mapStateToProps,
	null
)(DrawerMenu);
