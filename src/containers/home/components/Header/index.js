import BaseHeader from "../../../../base/Header/index";
import React, { PureComponent } from "react";

class Header extends PureComponent {
	render() {
		return (
			<BaseHeader
				title="RELAXING PLACES"
				// leftIcon="bars"
				// rightIcon="search"
				iconColor="gray"
				titleStyle={{
					color: "#56AFF8",
					fontWeight: "900",
					width: 200,
					textAlign: "center",
					letterSpacing: 0.7
				}}
				backgroundColor="white"
				// onPressLeftMenu={this.props.onPressLeftMenu}
				// onPressRightMenu={this.props.onPressRightMenu}
			/>
		);
	}
}

export default Header;
