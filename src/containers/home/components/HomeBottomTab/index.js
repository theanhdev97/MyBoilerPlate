import React, { Component } from "react";
import BaseTab from "../../../../base/Tab/index";
import HotTabs from "../../scenes/HotTabs/index";
import SearchNearbyTab from "../../scenes/SearchNearbyTab/index";
import CategoriesTab from "../../scenes/CategoriesTab/index";
import SearchTab from "../../scenes/SearchTab/index";

const ICONS = ["fire", "map", "list-alt", "search", "cogs"];
const ELEMENTS = [
	<HotTabs />,
	<SearchNearbyTab />,
	<CategoriesTab />,
	<SearchTab />,
	<HotTabs />
];

export default class HomeBottomTab extends Component {
	render() {
		return (
			<BaseTab
				activeColor={"#56AFF8"}
				unActiveColor={"gray"}
				elements={ELEMENTS}
				icons={ICONS}
				onChangeTab={this.props.onChangeTab}
				hasTitle={false}
			/>
		);
	}
}
