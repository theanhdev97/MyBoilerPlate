import React, { Component } from "react";
import { View, StatusBar, Text, TouchableOpacity } from "react-native";
import {
	Drawer,
	Header,
	Left,
	Body,
	Right,
	Title,
	Container,
	Icon
} from "native-base";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";

import * as Action from "../../actions/index";
// import * as Action from "../../actions"; //Import your actions
import style from "./style";
import DrawerMenu from "../home/components/drawer_menu/index";

import MyHeader from "./components/Header/index";

import HomeBottomTab from "./components/HomeBottomTab/index";

// REDUX
let context = "";
let currentScreen = "";

class Home extends Component {
	static _sShowTypeCoinDialog(screen) {
		currentScreen = screen;
		context.chooseTypeCoinDialog.show();
	}

	constructor(props) {
		super(props);
		context = this;
	}

	componentDidMount() {
		StatusBar.setHidden(true);
	}

	onPressLeftMenu = () => {
		alert("Press left menu ");
	};

	onPressRightMenu = () => {
		alert("Press right menu ");
	};

	onChangeTab = index => {
		// alert("Tab : " + index);
	};

	_handleOnPressDrawerMenu = () => {
		this.openDrawer();
	};

	_handleOnPressScanQRMenu = () => {
		Actions.ScanQR();
	};

	_handleChooseCoinType = coinType => () => {
		this.chooseTypeCoinDialog.dismiss();
		this.props.changeCoinType(currentScreen, coinType);
	};

	_onPressDrawerMenuItem = menu => {
		this.closeDrawer();
	};

	closeDrawer = () => {
		this.drawer._root.close();
		StatusBar.setHidden(false);
	};

	openDrawer = () => {
		this.drawer._root.open();
		StatusBar.setHidden(true);
	};

	renderDrawerMenu = () => (
		<DrawerMenu onPressDrawerMenuItem={this.onPressDrawerMenuItem} />
	);

	render() {
		return (
			<Drawer
				ref={ref => {
					this.drawer = ref;
				}}
				content={this.renderDrawerMenu()}
				onClose={this.closeDrawer}
				// {this.renderHeader()}
				// <MyHeader
				// 	onPressLeftMenu={this.onPressLeftMenu}
				// 	onPressRightMenu={this.onPressRightMenu}
				// />
			>
				<Container style={style.container}>
					<HomeBottomTab onChangeTab={this.onChangeTab} />
				</Container>
			</Drawer>
			// <HomeBottomTab />
		);
	}
}

function mapStateToProps(state, props) {
	return {
		title: state.changeTitleHomeReducer.title
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(Action, dispatch);
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home);
