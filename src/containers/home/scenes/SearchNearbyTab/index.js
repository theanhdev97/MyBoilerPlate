import React, { Component } from "react";
import { View, FlatList, Image } from "react-native";
import { Button, Text, Card, Icon } from "native-base";
import Ripple from "react-native-material-ripple";
import { Rating } from "react-native-elements";
import ListPlaces from '../../../../components/ListPlaces/index';
import {Actions} from 'react-native-router-flux';

export default class SearchNearbyTab extends Component {
	onPressPlaceItem = item => {
		Actions.Place({ data: item });
	};

	render() {
		return (
			<View style={{ flex: 1, backgroundColor: "white" }}>
				<ListPlaces
					onPressItem={this.onPressPlaceItem}
					type={1}
					latitude={10.79366}
					longtitude={106.722094}
					initialPage={0}
					isNearby={true}
				/>
			</View>
		);
	}
}
