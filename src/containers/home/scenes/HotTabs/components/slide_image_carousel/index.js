import Carousel from "react-native-snap-carousel";
import React, { Component } from "react";
import { View, Image, Text, Dimensions, ImageBackground } from "react-native";
import { Card, Icon } from "native-base";
import { Rating } from "react-native-elements";

export default class SlideImageCarousel extends Component {
  renderItem = item => (
    <Card
      style={{
        height: 200,
        width: "100%",
        paddingTop: 20,
        paddingBottom: 20,
        marginBottom: 20,
        alignItems: "center"
      }}
    >
      <Image
        source={{ uri: item.images['0'] }}
        style={{
          height: 200,
          width: "100%",
          borderRadius: 10
        }}
        resizeMode="cover"
      />
      <Card
        style={{
          position: "absolute",
          top: 0,
          left: -10,
          width: "90%",
          borderRadius: 10,
          padding: 10
        }}
      >
        <Text
          style={{
            color: "",
            fontSize: 17,
            fontWeight: "bold"
          }}
        >
          {item.title}
        </Text>

        <View
          style={{
            flexDirection: "row",
            marginTop: 4,
            alignItems: "center"
          }}
        >
          <Rating
            type="star"
            ratingCount={5}
            fractions={1}
            startingValue={item.rating}
            imageSize={11}
            onFinishRating={this.ratingCompleted}
            // showRating
            ratingColor="black"
            style={{ paddingVertical: 0 }}
          />
          <Text
            style={{
              marginLeft: 5,
              color: "#4ab8b8",
              fontSize: 11,
              fontWeight: "900"
            }}
          >
            {item.rating}
          </Text>
          <Text
            style={{
              color: "gray",
              fontWeight: "bold",
              fontSize: 10,
              marginLeft: 5
            }}
          >
            ( 260 reviews )
          </Text>
        </View>
      </Card>
    </Card>
    // <Text
    //   style={{
    //     marginTop: 8,
    //     color: "black",
    //     fontSize: 12
    //   }}
    // >
    //   {item.description}
    // </Text>
  );

  render() {
    let { data } = this.props;
    return (
      <Carousel
        ref={c => {
          this._carousel = c;
        }}
        loop
        enableSnap={true}
        data={data}
        autoplay
        autoplayDelay={2000}
        renderItem={({ item }) => this.renderItem(item)}
        sliderWidth={Dimensions.get("window").width}
        itemWidth={Dimensions.get("window").width * 0.8}
        // itemWidth={Dimensions.get("window").width * 0.8}
        // sliderWidth={300}
        // itemWidth={200}
      />
    );
  }
}
