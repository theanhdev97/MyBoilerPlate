import React, { Component } from "react";
import { View, FlatList, Image, ScrollView } from "react-native";
// import ImageLoad from "react-native-image-load";
import { Card, Text, Icon } from "native-base";
import { Rating } from "react-native-elements";
import SlideImageCarousel from "./components/slide_image_carousel/index";
import Ripple from "react-native-material-ripple";
import { Actions } from "react-native-router-flux";

import ListPlaces from "../../../../components/ListPlaces/index";
import { getPlaces } from "../../../../api/place";

let context;

export default class HotTab extends Component {
	constructor(props) {
		super(props);
		context = this;

		this.mListNewPlaces = [];
		this.mListHotPlaces = [];
	}

	componentDidMount() {
		// fetch list place NEWS
		// getPlaces(
		// 	1,
		// 	0,
		// 	10.79366,
		// 	106.722094,
		// 	this.onFetchNewPlacesSuccess,
		// 	this.onFetchFailure
		// );

		// fetch list place HOT
		getPlaces(
			2,
			0,
			10.79366,
			106.722094,
			this.onFetchHotPlacesSuccess,
			this.onFetchFailure
		);
	}

	onFetchNewPlacesSuccess = data => {
		context.mListNewPlaces.length = 0;
		context.mListNewPlaces.push(...data);
		context.forceUpdate();
	};

	onFetchHotPlacesSuccess = data => {
		context.mListHotPlaces.length = 0;
		context.mListHotPlaces.push(...data);
		context.forceUpdate();
	};

	onFetchFailure = error => {
		alert(error);
	};

	onPressPlaceItem = item => {
		Actions.Place({ data: item });
	};

	render() {
		// let { data } = this.props;
		let { mListNewPlaces, mListHotPlaces } = this;
		return (
			<ScrollView
				style={{
					flex: 1,
					// backgroundColor: "#D5D0D2"
					backgroundColor: "white",
					paddingTop: 10
				}}
			>
				<SlideImageCarousel data={mListHotPlaces} />
				<View style={{ flexDirection: "row", alignItems: "center" }}>
					<Text
						style={{
							color: "black",
							fontSize: 20,
							fontWeight: "bold",
							marginLeft: 10,
							marginTop: 20
						}}
					>
						Mới nhất
					</Text>
				</View>
				<View style={{ padding: 15, marginTop: 5 }}>
					<ListPlaces
						onPressItem={this.onPressPlaceItem}
						type={1}
						latitude={10.79366}
						longtitude={106.722094}
						initialPage={0}
					/>
				</View>
			</ScrollView>
		);
	}
	// <FlatList
	// 	contentContainerStyle={{
	// 		padding: 15,
	// 		paddingTop: 0,
	// 		marginTop: 5
	// 	}}
	// 	data={mListNewPlaces}
	// 	renderItem={({ item }) => this.renderListPlacesItem(item)}
	// 	keyExtractor={item => item.title}
	// 	showsVerticalScrollIndicator={false}
	// />
}
