import React, { Component } from "react";
import { View, FlatList, Dimensions } from "react-native";
import { Card, Text, Icon } from "native-base";
import Ripple from "react-native-material-ripple";
import { Actions } from "react-native-router-flux";

const data = [
	{
		type: "sport",
		name: "Thể thao",
		icon: {
			type: "FontAwesome",
			name: "spotify"
		},
		color: "#ff7a5c"
	},
	{
		type: "bar",
		name: "Quán bar",
		icon: {
			type: "FontAwesome",
			name: "beer"
		},
		color: "#53d397"
	},
	{
		type: "dating",
		name: "Hẹn hò",
		icon: {
			type: "FontAwesome",
			name: "gratipay"
		},
		color: "#ffd3ad"
	},
	{
		type: "coffee",
		name: "Cà phê",
		icon: {
			type: "FontAwesome",
			name: "coffee"
		},
		color: "#2aa9d2"
	},
	{
		type: "movie",
		name: "Xem phim",
		icon: {
			type: "FontAwesome",
			name: "film"
		},
		color: "#a1c45a"
	},
	{
		type: "selfy",
		name: "Selfy",
		icon: {
			type: "FontAwesome",
			name: "camera-retro"
		},
		color: "#facf5a"
	},
	{
		type: "bar",
		name: "Quán bar",
		icon: {
			type: "FontAwesome",
			name: "wine-glass-alt"
		},
		color: "#e6e6d4"
	},
	{
		type: "bar",
		name: "Quán bar",
		icon: {
			type: "FontAwesome",
			name: "wine-glass-alt"
		},
		color: "#f2c6b4"
	}
];

export default class CategoriesTab extends Component {
	handleOnPressItem = item => {
		Actions.PlacesWithCategory({ category: 3, title: item.name });
		// alert("item: " + item.name);
	};

	renderCategoryItem = item => (
		<Ripple
			rippleCentered={true}
			onPress={() => this.handleOnPressItem(item)}
			rippleDuration={1000}
		>
			<Card
				style={{
					backgroundColor: item.color,
					marginRight: 5,
					height: 150,
					borderRadius: 8,
					alignItems: "center",
					justifyContent: "center",
					width: Dimensions.get("window").width / 2 - 20
				}}
			>
				<Icon
					name={item.icon.name}
					type={item.icon.type}
					style={{ fontSize: 50, color: "white" }}
				/>
				<Text
					style={{
						marginTop: 10,
						fontSize: 16,
						fontWeight: "bold",
						color: "white"
					}}
				>
					{item.name}
				</Text>
			</Card>
		</Ripple>
	);

	render() {
		// let { data } = this.props;
		return (
			<View
				style={{
					flex: 1,
					// backgroundColor: "#D5D0D2"
					backgroundColor: "white"
				}}
			>
				<FlatList
					contentContainerStyle={{
						padding: 5,
						flexDirection: "row",
						flexWrap: "wrap",
						justifyContent: "center"
					}}
					data={data}
					renderItem={({ item }) => this.renderCategoryItem(item)}
					keyExtractor={item => item.type}
					showsVerticalScrollIndicator={false}
				/>
			</View>
		);
	}
}
