import { StyleSheet } from "react-native";

export default StyleSheet.create({
	container: {
		backgroundColor: "white",
		flex: 7
	},
	logoContainer: {
		flex: 2,
		// justifyContent: "center",
		alignItems: "center",
		justifyContent: "flex-end"
	},
	logo: { width: 200, height: 100 },
	formContainer: {
		flex: 4,
		justifyContent: "center"
	},
	formEmailPasswordContainer: {
		margin: 20,
		marginRight: 35,
		alignItems: "center"
	},
	inputEmailContianer: {
		marginBottom: 10
	},
	inputText: {
		fontSize: 18
	},
	signInButton: { marginLeft: 20, marginRight: 20 },
	signInButtonText: { color: "#FFF3E0" },
	forgotPasswordContainer: {
		marginTop: 5,
		width: "100%",
		// backgroundColor: 'pink',
		// width: 150,
		marginLeft: 0,
		paddingLeft: 0,
		justifyContent: "center"
	},
	forgotPasswordText: {
		textAlign: "center",
		color: "orange"
	},
	registerContainer: {
		flexDirection: "row",
		paddingLeft: 20,
		justifyContent: "center",
		marginBottom: 10
	},
	dontHaveAccountText: { color: "black" },
	registerText: {
		color: "orange",
		marginLeft: 10
	},
	footerContainer: {
		marginLeft: 80,
		marginRight: 80,
		marginBottom: 20,
		flex: this.mFooterHeight,
		justifyContent: "space-between",
		alignItems: "flex-end",
		flexDirection: "row"
	},
	forumText: {
		color: "gray",
		textAlign: "center",
		marginBottom: 5,
		fontWeight: "bold"
	},
	englishButtonContainer: { flexDirection: "row" },
	englishButtonText: {
		color: "gray",
		textAlign: "center",
		marginRight: 10,
		fontWeight: "bold"
	},
	sendingOverlayParentContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	sendingOverlayChildContainer: {
		backgroundColor: "white",
		justifyContent: "center",
		alignItems: "center",
		width: 300,
		height: 200,
		borderRadius: 5,
		borderWidth: 1,
		borderColor: "black"
	},
	sendingOverlayText: {
		marginTop: 10,
		fontSize: 30,
		color: "orange"
	}
});
