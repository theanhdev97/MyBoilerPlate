import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Text} from 'native-base';
// import {Actions} from 'react-native-router-flux';

// REDUX
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {updateToken, clearToken} from '../../actions/index';

import style from './style';
import withLoadingAndRefreshingScrollView
  from '../../base/withLoadingAndRefreshingScrollView';

import {getToken, getUserInfo} from '../../selectors/userSelector';

let context;

const fetchData = async (_onSuccess, _onFailure) => {
  try {
    let result = await fetch ('https://api.github.com');
    let resultJson = await result.json ();
    _onSuccess (resultJson);
  } catch (err) {
    _onFailure (err.toString ());
  }
};

class Login extends Component {
  constructor (props) {
    super (props);
    context = this;
  }

  onFetchDataSuccess = () => {
    alert ('call cComponent fetch success');
  };

  onFetchDataFailure = () => {};

  render () {
    return (
      <View>
        <Text style={{color: 'red', fontSize: 20}}>
          TOKEN : {this.props.token}
        </Text>
        <Text>{JSON.stringify (this.props.data)}</Text>
      </View>
    );
  }
}
// // === NOT USE SELECTORS ===
// mapStateToProps = state => {
//   return {
//     token: state.user.token,
//     userInfo: state.user.userInfo,
//   };
// };

// === USE SELECTORS ===
mapStateToProps = state => {
  return {
    token: getToken (state),
    userInfo: getUserInfo (state),
  };
};

// ---- C1 : Import những actions cần thiết
mapDispatchToProps = dispatch => ({
  updateToken: token => dispatch (updateToken (token)),
  clearToken: () => dispatch (clearToken ()),
});

// ---- C2 : Import toàn bộ actions

// function mapDispatchToProps (dispatch) {
//   return bindActionCreators (Action, dispatch);
// }

// Connect everything
// export default connect (mapStateToProps, mapDispatchToProps) (Login);

const LoginWithLoadingAndRefreshingScrollView = withLoadingAndRefreshingScrollView (
  Login,
  fetchData
);

export default connect (mapStateToProps, mapDispatchToProps) (
  LoginWithLoadingAndRefreshingScrollView
);
