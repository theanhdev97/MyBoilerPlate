import React from 'react';
import { View, Text } from 'react-native';

interface AppProps {
    id: number
}

interface AppState {
    token: string
}

export default class AppComponent extends React.Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);
        this.state = {
            token: '1'
        };
    }


    componentDidMount() {
        this.hello();
    }

    hello(): void {
        alert("Hello with TypeScript")
    }

    render() {
        return (
            <View>
                <Text>hello</Text>
            </View>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        abc: state.user.asfd
    }
}