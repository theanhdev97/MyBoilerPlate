import PropTypes from "prop-types";
import React, { Component } from "react";
import {
	View,
	Image,
	TouchableOpacity,
	Dimensions,
	Platform,
	Linking
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import {
	Header,
	Container,
	Left,
	Body,
	Title,
	Icon,
	Right,
	Button,
	Text
} from "native-base";
import { Actions } from "react-native-router-flux";

class GoogleMap extends Component {
	static propTypes = {
		title: PropTypes.string,
		subTitle: PropTypes.string,
		latitude: PropTypes.any,
		longitude: PropTypes.any
	};

	handlePressBack = () => {
		Actions.pop();
	};

	handleOnMapReady = () => {
		this.maker.showCallout();
	};

	handlePressDirections = () => {
		var url = "geo:37.484847,-122.148386";

		if (Platform.OS === "ios") {
			Linking.openURL(`http://maps.apple.com/?daddr=${url}`);
		} else {
			Linking.openURL(`http://maps.google.com/?daddr=${url}`);
		}
	};

	renderHeader = () => (
		<Header style={{ backgroundColor: "#f73859" }}>
			<Left>
				<TouchableOpacity onPress={() => this.handlePressBack()}>
					<Icon
						name="arrow-left"
						type="FontAwesome"
						style={{ color: "white", fontSize: 20 }}
					/>
				</TouchableOpacity>
			</Left>
			<Body>
				<Title style={{ fontSize: 18, width: 200, color: "white" }}>
					RELAXING PLACES
				</Title>
			</Body>
			<Right />
		</Header>
	);

	renderDirectionButton = () => (
		<View
			style={{
				position: "absolute",
				bottom: 20,
				left: 0,
				width: Dimensions.get("window").width,
				alignItems: "center",
				justifyContent: "center"
			}}
		>
			<TouchableOpacity onPress={() => this.handlePressDirections()}>
				<Button
					style={{ color: "white" }}
					onPress={() => this.handlePressDirections()}
				>
					<Icon
						name="compass"
						type="FontAwesome"
						style={{ fontSize: 20 }}
					/>
					<Text style={{ marginLeft: -20 }}>Chỉ đường</Text>
				</Button>
			</TouchableOpacity>
		</View>
	);

	render() {
		let { title, subTitle, latitude, longitude } = this.props;

		return (
			<Container style={{ flex: 1, backgroundColor: "pink" }}>
				{this.renderHeader()}
				<MapView
					style={{ flex: 1 }}
					initialRegion={{
						latitude: latitude,
						longitude: longitude,
						latitudeDelta: 0.00922,
						longitudeDelta: 0.00421
					}}
					onMapReady={this.handleOnMapReady}
				>
					<Marker
						ref={ref => (this.maker = ref)}
						coordinate={{
							latitude: latitude,
							longitude: longitude,
							latitudeDelta: 0.00922,
							longitudeDelta: 0.00421
						}}
						title={title}
						description={subTitle}
					>
						<Icon
							type="FontAwesome"
							name="map-marker"
							style={{ marginLeft: 23, width: 50, height: 50 }}
						/>
					</Marker>
				</MapView>
				{this.renderDirectionButton()}
			</Container>
		);
	}
}

export default GoogleMap;
