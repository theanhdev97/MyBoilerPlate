import {
  CALL_UPDATE_TOKEN,
  UPDATE_TOKEN,
  CALL_CLEAR_TOKEN,
  CLEAR_TOKEN,
  CALL_UPDATE_USER_INFO,
  UPDATE_USER_INFO,
} from '../actions/action_types';

import {all, put} from 'redux-saga/effects';
import {takeEvery, takeLatest} from 'redux-saga';

const delay = ms => new Promise (res => setTimeout (res, ms));

// =========== FUNCTION ==============

function* updateToken (action) {
  yield put ({
    type: UPDATE_TOKEN,
    token: action.payload.token,
  });
}

function* clearToken () {
  yield put ({
    type: CLEAR_TOKEN,
  });
}

function* updateUserInfo (action) {
  yield put ({
    type: UPDATE_USER_INFO,
    userInfo: action.payload.userInfo,
  });
}

// ------ WATCH ------------

function* watchAll () {
  yield takeEvery (CALL_UPDATE_TOKEN, updateToken);
  yield takeEvery (CALL_CLEAR_TOKEN, clearToken);
  yield takeEvery (CALL_UPDATE_USER_INFO, updateUserInfo);
}

export default watchAll;
