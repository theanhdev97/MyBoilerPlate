export function random() {
	return Math.floor(Math.random() * 89 + 10);
}

export function roundingNumber(_number, _offset = 2) {
	return (Math.round(_number * 100) / 100).toFixed(_offset);
}
