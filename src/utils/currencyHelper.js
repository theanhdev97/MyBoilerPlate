import {Platform} from 'react-native';

export function formatCurrency (_number) {
  // return Number (_number.toFixed (1)).toLocaleString ();
  // return Number (parseFloat (_number).toFixed (1)).toLocaleString ();
  if (Platform.OS === 'ios')
    return (_number).toLocaleString (fa ? 'fa-IR' : 'en-US', {
      maximumFractionDigits: 0,
    });
  else
    return (_number)
      .toString ()
      .replace (/\B(?=(\d{3})+(?!\d))/g, ',');
  // return 1;
}
