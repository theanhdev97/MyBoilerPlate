export function removeItemByKey (_array, _key, _value) {
  let index = _array.findIndex (item => item[_key] === _value);
  _array.splice (index, 1);
  return _array;
}

export function removeAllItemByKey (_array, _key, _value) {
  let indexs = [];
  _array.map ((item, index) => {
    if (item[_key] === _value) indexs.push (index);
  });
  indexs.reverse ();
  indexs.forEach (item => _array.splice (item, 1));

  return _array;
}

export function getDistinctArray (_array) {
  var unique = {};
  var distinct = [];
  for (var i in _array) {
    if (typeof unique[_array[i].id] == 'undefined') {
      distinct.push (_array[i]);
    }
    unique[_array[i].id] = 0;
  }
  return distinct;
}
