export function getCurrentDate() {
	var x = new Date();
	var y = x.getFullYear().toString();
	var m = (x.getMonth() + 1).toString();
	var d = x.getDate().toString();
	d.length == 1 && (d = "0" + d);
	m.length == 1 && (m = "0" + m);
	var date = d + "-" + m + "-" + y;
	return date;
}

export function convertMillisecondToTime(_millisecondTime) {
	let result = "";
	let second = _millisecondTime / 1000;

	let minute = second / 60;
	let hour = second / ( 60 * 60);
	let day = second / ( 60 * 60 * 24);

	if(minute < 1)
		return `${second} giây`;
	if(minute < 60)
		return `${minute} phút`;
	if(hour < 24)
		return `${hour} giờ`
	return `${day} ngày`
}
