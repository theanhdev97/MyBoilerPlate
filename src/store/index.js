import {createStore, applyMiddleware, compose} from 'redux';
import {persistStore, persistCombineReducers} from 'redux-persist';
import storage from 'redux-persist/es/storage'; // default: localStorage if web, AsyncStorage if react-native
// import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import reducers from '../reducers';
import {composeWithDevTools} from 'remote-redux-devtools';

import rootSaga from '../saga/index';
// import fetchSaga from '../saga/fetchSaga';

// Redux Persist config
const config = {
  key: 'root',
  storage,
  whitelist: ['user'],
};

const reducer = persistCombineReducers (config, reducers);

// const middleware = [thunk];

const sagaMiddleware = createSagaMiddleware ();

const configureStore = () => {
  const store = createStore (
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__ (),
    // compose(applyMiddleware(...middleware))
    // composeWithDevTools (applyMiddleware (...middleware))
    composeWithDevTools (applyMiddleware (sagaMiddleware))
  );

  const persistor = persistStore (store, null, () => {
    store.getState ();
  });
  sagaMiddleware.run (rootSaga);
  // sagaMiddleware.run (rootSaga).done.catch (error => alert (error));
  // store.runSaga (rootSaga);

  return {persistor, store};
};

export default configureStore;
