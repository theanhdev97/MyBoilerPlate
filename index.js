import {AppRegistry} from 'react-native';
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import Routes from './src/routes/index';
import configureStore from './src/store/index';
const {persistor, store} = configureStore ();
// import {PersistGate} from 'redux-persist/integration/react';
import {PersistGate} from 'redux-persist/es/integration/react';

//
const Index = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <Routes />
    </PersistGate>
  </Provider>
);

AppRegistry.registerComponent ('app_wallet', () => Index);
